//
//  Queue.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import "Queue.h"

@implementation Queue

+ (instancetype)commandWithDirection:(MoveDirection)direction
                     completionBlock:(void(^)(BOOL))completion {
    Queue *command = [[self class] new];
    command.direction = direction;
    command.completion = completion;
    return command;
}

@end


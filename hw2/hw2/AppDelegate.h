//
//  AppDelegate.h
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/15/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


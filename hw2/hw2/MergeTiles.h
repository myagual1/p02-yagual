//
//  MergeTiles.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    MergeTileEmpty = 0,
    MergeTileNoAction,
    MergeTileMove,
    MergeTileSingleCombine,
    MergeTileDoubleCombine
}MergeTileMode;

@interface MergeTile : NSObject

@property (nonatomic) MergeTileMode mode;
@property (nonatomic) NSInteger originalIndexA;
@property (nonatomic) NSInteger originalIndexB;
@property (nonatomic) NSInteger value;

+ (instancetype)mergeTile;

@end

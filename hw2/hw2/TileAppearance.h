//
//  TileApperance.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol TileAppearanceProtocol <NSObject>

- (UIColor *)tileColorForValue:(NSUInteger)value;
- (UIColor *)numberColorForValue:(NSUInteger)value;
- (UIFont *)fontForNumbers;

@end

@interface TileAppearance : NSObject <TileAppearanceProtocol>

@end

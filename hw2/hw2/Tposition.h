//
//  NSObject_position.h
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#ifndef Tposition_h
#define Tposition_h

typedef struct Position {
    NSInteger x;
    NSInteger y;
} VTposition;

CG_INLINE VTposition changePos(NSInteger x, NSInteger y) {
    VTposition pos;
    pos.x = x; pos.y = y;
    return pos;
}

#endif

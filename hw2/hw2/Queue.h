//
//  Queue.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameStruct.h"

@interface Queue: NSObject

@property (nonatomic) MoveDirection direction;
@property (nonatomic, copy) void(^completion)(BOOL atLeastOneMove);

+ (instancetype)commandWithDirection:(MoveDirection)direction completionBlock:(void(^)(BOOL))completion;

@end

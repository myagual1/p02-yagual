//
//  ViewController.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/15/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import "ViewController.h"
#import "TileGameViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (IBAction)startedButtonClicked:(id)sender{
    TileGameViewController *c = [TileGameViewController numberTileGameWithDimension:4
                                                                                         winThreshold:2048
                                                                                      backgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"back.jpg"]]
                                                                                          scoreModule:YES
                                                                                       buttonControls:NO
                                                                                        swipeControls:YES];
    [self presentViewController:c animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.jpg"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

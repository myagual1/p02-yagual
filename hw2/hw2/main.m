//
//  main.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/15/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  ControlView.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ControlViewProtocol

- (void)upButtonTapped;
- (void)downButtonTapped;
- (void)leftButtonTapped;
- (void)rightButtonTapped;
- (void)resetButtonTapped;
- (void)exitButtonTapped;

@end

@interface ControlView : UIView

+ (instancetype)controlViewWithCornerRadius:(CGFloat)radius
                            backgroundColor:(UIColor *)color
                            movementButtons:(BOOL)moveButtonsEnabled
                                 exitButton:(BOOL)exitButtonEnabled
                                   delegate:(id<ControlViewProtocol>)delegate;

@end

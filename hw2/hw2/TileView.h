//
//  TileView.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TileAppearance.h"

@protocol TileAppearanceProviderProtocol;
@interface TileView : UIView

@property (nonatomic) NSInteger tileValue;

@property (nonatomic, weak) id<TileAppearanceProtocol>delegate;

+ (instancetype)tileForPosition:(CGPoint)position
                     sideLength:(CGFloat)side
                          value:(NSUInteger)value
                   cornerRadius:(CGFloat)cornerRadius;

@end


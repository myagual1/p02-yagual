//
//  GameStruct.h
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    MoveUp = 0,
    MoveDown,
    MoveLeft,
    MoveRight
} MoveDirection;

@protocol GameStructProtocol

- (void)scoreChanged:(NSInteger)newScore;
- (void)moveTileFromIndexPath:(NSIndexPath *)fromPath
                  toIndexPath:(NSIndexPath *)toPath
                     newValue:(NSUInteger)value;
- (void)moveTileOne:(NSIndexPath *)startA
            tileTwo:(NSIndexPath *)startB
        toIndexPath:(NSIndexPath *)end
           newValue:(NSUInteger)value;
- (void)insertTileAtIndexPath:(NSIndexPath *)path
                        value:(NSUInteger)value;

@end

@interface GameStruct : NSObject

@property (nonatomic, readonly) NSInteger score;

+ (instancetype)gameStructWithDimesion: (NSUInteger)dimension
                    winValue:(NSUInteger)val
                    delegate:(id<GameStructProtocol>) delegate;

- (void)reset;
- (void)insertAtRandomLocationTileWithValue:(NSUInteger)value;

- (void)insertTileWithValue:(NSUInteger)value
                atIndexPath:(NSIndexPath *)path;

- (void)performMoveInDirection:(MoveDirection)direction
               completionBlock:(void(^)(BOOL))completion;

- (BOOL)userHasLost;
- (NSIndexPath *)userHasWon;

@end

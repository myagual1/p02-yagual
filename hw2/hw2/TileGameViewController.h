//
//  NSObject+TileGameViewController.h
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol TileGameProtocol <NSObject>
//inform that the game ended

- (void)gameEndVictory:(BOOL)didWin
                 score:(NSInteger)score;

@end


@interface TileGameViewController : UIViewController

@property (nonatomic, weak) id<TileGameProtocol>delegate;

+ (instancetype)numberTileGameWithDimension:(NSUInteger)dimension
                               winThreshold:(NSUInteger)threshold
                            backgroundColor:(UIColor *)backgroundColor
                                scoreModule:(BOOL)scoreModuleEnabled
                             buttonControls:(BOOL)buttonControlsEnabled
                              swipeControls:(BOOL)swipeControlsEnabled;

@end

//
//  MergeTile.m
//  hw2
//
//  Created by Mariuxi Ana Yagual on 3/16/17.
//  Copyright © 2017 Binghamton University. All rights reserved.
//

#import "MergeTiles.h"

@implementation MergeTile

+ (instancetype)mergeTile {
    return [[self class] new];
}

- (NSString *)description {
    NSString *modeStr;
    switch (self.mode) {
        case MergeTileEmpty:
            modeStr = @"Empty";
            break;
        case MergeTileNoAction:
            modeStr = @"NoAction";
            break;
        case MergeTileMove:
            modeStr = @"Move";
            break;
        case MergeTileSingleCombine:
            modeStr = @"SingleCombine";
            break;
        case MergeTileDoubleCombine:
            modeStr = @"DoubleCombine";
    }
    return [NSString stringWithFormat:@"MergeTile (mode: %@, source1: %ld, source2: %ld, value: %ld)",
            modeStr,
            (long)self.originalIndexA,
            (long)self.originalIndexB,
            (long)self.value];
}


@end

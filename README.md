#Project 2: 2048 
Author: Mariuxi Yagual

This project is modeled after 2048.
I used the skeleton code found in https://github.com/austinzheng/iOS-2048 to learn about the animation used while the tiles merge.
I still have more to learn about animating backgrounds and titles to make my app prettier.